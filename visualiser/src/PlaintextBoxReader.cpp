#include "PlaintextBoxReader.hpp"

#include <sstream>

std::vector<std::string> splitByWhitespace(const std::string &s)
{
    std::stringstream ss(s);
    std::string token;

    std::vector<std::string> tokens;

    while (std::getline(ss, token, ' '))
    {
        tokens.push_back(token);
    }

    return tokens;
}

void handleBoxLine(const std::string& line, std::unique_ptr<Box>& box)
{
    std::vector<std::string> elements = splitByWhitespace(line);

    box->setWidth(std::stof(elements[1]));
    box->setHeight(std::stof(elements[2]));
    box->setDepth(std::stof(elements[3]));
    box->setPeriodicX(std::stoi(elements[4]));
    box->setPeriodicY(std::stoi(elements[5]));
    box->setPeriodicZ(std::stoi(elements[6]));
}

void handleFibreLine(const std::string& line, std::unique_ptr<Box>& box)
{
    std::vector<std::string> elements = splitByWhitespace(line);

    glm::vec3 start = { std::stof(elements[2]), std::stof(elements[3]), std::stof(elements[4]) };
    glm::vec3 end = { std::stof(elements[5]), std::stof(elements[6]), std::stof(elements[7]) };
    float radius = std::stof(elements[8]);

    Fibre fibre(start, end, radius);
    box->addFibre(fibre);
}

void handleNodeLine(const std::string& line, std::unique_ptr<Box>& box)
{
    std::vector<std::string> elements = splitByWhitespace(line);

    unsigned id = std::stoi(elements[1]);
    glm::vec3 pos = { std::stof(elements[2]), std::stof(elements[3]), std::stof(elements[4]) };

    for (unsigned i = 5; i < elements.size(); ++i)
    {
        Node node(pos);
        unsigned parentFibre = std::stoi(elements[i]);
        box->getFibres()[parentFibre].addNode(node);
    }
}

std::unique_ptr<Box> PlaintextBoxReader::read(const std::string& filename)
{
    file.open(filename);

    if (!file.is_open())
    {
        std::cerr << "PlaintextBoxWriter::write: unable to open file \"" << filename << "\"" << std::endl;
        return nullptr;
    }

    std::unique_ptr<Box> box(new Box({1.f, 1.f, 1.f}, 1.f));

    std::string line;
    while (std::getline(file, line))
    {
        switch (line[0])
        {
            case 'b': 
                handleBoxLine(line, box);
                break;
            case 'f': 
                handleFibreLine(line, box);
                break;
            case 'n':
                handleNodeLine(line, box);
            default:
                break;
        }
    }

    file.close();

    return box;
}
