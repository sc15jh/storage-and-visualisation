#include "PNGScreenGrabber.hpp"

#include <png.h>
#include <cstdio>
#include <iostream>
#include <GLInclude.hpp>

PNGScreenGrabber::PNGScreenGrabber()
    : PNGScreenGrabber(SCREENGRABBER_DEFAULT_PREFIX)
{
}

PNGScreenGrabber::PNGScreenGrabber(const std::string& prefix)
    : ScreenGrabber(prefix)
{
}

void PNGScreenGrabber::grab()
{
    std::string filename = prefix + std::to_string(suffix) + std::string(".png");
    FILE* fp = std::fopen(filename.c_str(), "wb");
    if (fp == nullptr)
    {
        std::cerr << "Unable to create file: " << filename << std::endl;
    }

    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    png_infop info_ptr = png_create_info_struct(png_ptr);

    if (setjmp(png_jmpbuf(png_ptr)))
    {
        std::cerr << "Error occured when calling png_init_io" << std::endl;
        png_destroy_write_struct(&png_ptr, &info_ptr);
        fclose(fp);
        return;
    }
    
    png_init_io(png_ptr, fp);

    if (setjmp(png_jmpbuf(png_ptr)))
    {
        std::cerr << "Error occured when writing the png header to " << filename << std::endl;
        return;
    }

    int width = glutGet(GLUT_WINDOW_WIDTH);
    int height = glutGet(GLUT_WINDOW_HEIGHT);
    int depth;
    glGetIntegerv(GL_DEPTH_BITS, &depth);

    png_set_IHDR(png_ptr, info_ptr, width, height,
            8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
            PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    png_write_info(png_ptr, info_ptr); 

    if (setjmp(png_jmpbuf(png_ptr)))
    {
        std::cerr << "Error occured when writing pixel bytes to " << filename << std::endl;
        return;
    }

    uint8_t* pixels = new uint8_t[width * height * 3];
    glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, (GLvoid*) pixels);

    png_bytep* rows = new png_bytep[height];
    for (int i = 0; i < height; ++i)
    {
        rows[i] = (png_bytep) (pixels + (height - i - 1) * width * 3);
    }

    png_write_image(png_ptr, rows);

    if (setjmp(png_jmpbuf(png_ptr)))
    {
        std::cerr << "Error occured when writing end of " << filename << std::endl;
        return;
    }

    png_write_end(png_ptr, NULL);

    delete [] rows;
    delete [] pixels;

    ++suffix;
}
