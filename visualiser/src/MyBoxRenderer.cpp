#include "GLInclude.hpp"
#include "MyBoxRenderer.hpp"

void MyBoxRenderer::render(Box& b)
{
    glLineWidth(2.f);
    glColor3f(1.f, 1.f, 1.f);
    glBegin(GL_LINE_LOOP);
    // Front face
    glVertex3f(-1.f, -1.f, 1.f);
    glVertex3f(1.f, -1.f, 1.f);
    glVertex3f(1.f, 1.f, 1.f);
    glVertex3f(-1.f, 1.f, 1.f);
    glEnd();

    // If the box is 2D, we don't need to render any more so return.
    if (b.getDepth() == 0.f)
        return;

    glBegin(GL_LINE_LOOP);
    // Back face
    glVertex3f(-1.f, -1.f, -1.f);
    glVertex3f(1.f, -1.f, -1.f);
    glVertex3f(1.f, 1.f, -1.f);
    glVertex3f(-1.f, 1.f, -1.f);
    glEnd();

    glBegin(GL_LINE_LOOP);
    // Right face
    glVertex3f(1.f, -1.f, 1.f);
    glVertex3f(1.f, -1.f, -1.f);
    glVertex3f(1.f, 1.f, -1.f);
    glVertex3f(1.f, 1.f, 1.f);
    glEnd();

    glBegin(GL_LINE_LOOP);
    // Left face
    glVertex3f(-1.f, -1.f, 1.f);
    glVertex3f(-1.f, -1.f, -1.f);
    glVertex3f(-1.f, 1.f, -1.f);
    glVertex3f(-1.f, 1.f, 1.f);
    glEnd();

    glBegin(GL_LINE_LOOP);
    // Top face
    glVertex3f(-1.f, 1.f, 1.f);
    glVertex3f(1.f, 1.f, 1.f);
    glVertex3f(1.f, 1.f, -1.f);
    glVertex3f(-1.f, 1.f, -1.f);
    glEnd();

    glBegin(GL_LINE_LOOP);
    // Bottom face
    glVertex3f(-1.f, -1.f, 1.f);
    glVertex3f(1.f, -1.f, 1.f);
    glVertex3f(1.f, -1.f, -1.f);
    glVertex3f(-1.f, -1.f, -1.f);
    glEnd();
}
