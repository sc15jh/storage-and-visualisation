#include "GLInclude.hpp"
#include "MyFibreRenderer.hpp"

#include <vector>

float scaleX(Box& b, float x) { return (2.f*x) / b.getWidth() - 1.f; }
float scaleY(Box& b, float y) { return (2.f*y) / b.getHeight() - 1.f; }
float scaleZ(Box& b, float z) { return b.getDepth() == 0.f ? 1.f : (2.f*z) / b.getDepth() - 1.f; }

/**
 * \brief Color the fibres based on their depth if 3D, or their x-position if 2D.
 *
 * \param b The box containing the fibres.
 * \param f The fibre to color.
 */
void setColor(Box& b, Fibre& f)
{
    float distance = (b.getDepth() == 0.f ? f.start.x / b.getWidth() : f.start.z / b.getDepth());
    float freq = 5.f;
    glColor3f((std::sin(distance*freq + 0) * 127.f + 128.f) / 255.f, (std::sin(distance*freq + 2) * 127.f + 128.f) / 255.f, (std::sin(distance*freq + 4) * 127.f + 128.f) / 255.f);
}

void MyFibreRenderer::render(Box& b)
{
    std::vector<glm::vec3> nodes;

    glBegin(GL_LINES);
    for (auto& f : b.getFibres())
    {
        setColor(b, f);
        glVertex3f(scaleX(b, f.start.x), scaleY(b, f.start.y), scaleZ(b, f.start.z));
        glVertex3f(scaleX(b, f.end.x), scaleY(b, f.end.y), scaleZ(b, f.end.z));

        for (auto& n : f.getNodes())
            nodes.push_back(n.getPosition());
    }
    glEnd();

    glPointSize(2.f);
    glDisable(GL_DEPTH_TEST);
    glColor3f(1.f, 1.f, 1.f);
    glBegin(GL_POINTS);
    for (auto& n : nodes)
        glVertex3f(scaleX(b, n.x), scaleY(b, n.y), scaleZ(b, n.z));
    glEnd();
    glEnable(GL_DEPTH_TEST);
}
