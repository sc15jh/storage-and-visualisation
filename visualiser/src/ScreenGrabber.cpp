#include "ScreenGrabber.hpp"

ScreenGrabber::ScreenGrabber()
    : ScreenGrabber(SCREENGRABBER_DEFAULT_PREFIX)
{
}

ScreenGrabber::ScreenGrabber(const std::string& prefix)
    : prefix(prefix),
      suffix(0)
{
}
