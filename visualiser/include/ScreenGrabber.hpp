#ifndef SCREEN_GRABBER_HPP
#define SCREEN_GRABBER_HPP

#include <string>

#define SCREENGRABBER_DEFAULT_PREFIX "screengrab"

/**
 * \brief Handles writing the pixels on the screen to a file.
 */
class ScreenGrabber
{
    public:
        /**
         * \brief Default constructor. The prefix of the filenames is the default prefix.
         */
        ScreenGrabber();

        /**
         * \brief Construct a \c ScreenGrabber instance which uses the given prefix.
         * \param prefix The prefix for output filenames.
         */
        ScreenGrabber(const std::string& prefix);

        /**
         * \brief Writes the pixels on the screen to a file.
         */
        virtual void grab() = 0;

    protected:
        std::string prefix; ///< The name of the output files.
        unsigned suffix; ///< A counter that is appended to the output filenames.
};

#endif
