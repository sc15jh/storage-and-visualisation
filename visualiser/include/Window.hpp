#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <string>
#include <functional>

using DisplayCallback = void();
using KeyboardCallback = void(unsigned char, int, int);
using SpecialKeyboardCallback = void(int, int, int);
using MotionCallback = void(int, int);
using IdleCallback = void();
using MouseCallback = void(int, int, int, int);

/**
 * \brief A class containing functions relating to the window to abstract GLUT.
 */
class Window
{
    public:
        /**
         * \brief Constructs a window with the given parameters.
         *
         * \param width The width of the window.
         * \param height The height of the window.
         * \param title The title of the window.
         */
        Window(int width, int height, const std::string& title);

        /**
         * \brief Sets the display callback function for this window.
         * \param display A function pointer to the display callback.
         */
        void setDisplayCallback(DisplayCallback*);

        /**
         * \brief Sets the keyboard callback function for this window.
         * \param keyboard A function pointer to the keyboard callback.
         */
        void setKeyboardCallback(KeyboardCallback*);

        /**
         * \brief Sets the special keyboard callback function (i.e. handles non-ascii characters) for this window.
         * \param specialKeyboard A function pointer to the special keyboard callback.
         */
        void setSpecialKeyboardCallback(SpecialKeyboardCallback*);

        /**
         * \brief Sets the motion callback function for this window.
         * \param motion A function pointer to the motion callback.
         */
        void setMotionCallback(MotionCallback*);

        /**
         * \brief Sets the idle callback function for this window.
         * \param idle A function pointer to the idle callback.
         */
        void setIdleCallback(IdleCallback*);

        /**
         * \brief Sets the mouse callback function for this window.
         * \param mouse A function pointer to the mouse callback.
         */
        void setMouseCallback(MouseCallback*);

        /**
         * \brief Initializes OpenGL and sets up the coordinate system.
         * \note This must be called before \c start().
         */
        void initGL() const;
        
        /**
         * \brief Starts the main loop.
         * \note This must be called, otherwise the window will be created and then destroyed.
         */
        void start() const;

    private:
        DisplayCallback* display; ///< Function pointer to the display callback.
        KeyboardCallback* keyboard; ///< Function pointer to the keyboard callback.
        SpecialKeyboardCallback* specialKeyboard; ///< Function pointer to the special keyboard callback.
        MotionCallback* motion; ///< Function pointer to the motion callback.
        IdleCallback* idle; ///< Function pointer to the idle callback.
        MouseCallback* mouse; ///< Function pointer to the mouse callback.
};

#endif
