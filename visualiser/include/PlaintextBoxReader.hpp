#ifndef PLAINTEXT_BOX_READER_HPP
#define PLAINTEXT_BOX_READER_HPP

#include "BoxReader.hpp"

/**
 * \brief Handles reading plaintext data about a \c Box instance from a file
 */
class PlaintextBoxReader : public BoxReader
{
    public:
        /**
         * \brief A concrete implementation of the pure virtual function declared
         * by \c BoxReader.
         *
         * \param filename The name of the file that is to be read from.
         *
         * \return A pointer to the box.
         */
        std::unique_ptr<Box> read(const std::string& filename);
};

#endif
