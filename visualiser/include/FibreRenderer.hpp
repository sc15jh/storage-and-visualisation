#ifndef FIBRE_RENDERER_HPP
#define FIBRE_RENDERER_HPP

#include "Box.hpp"

/**
 * \brief Handles rendering a \c Fibre.
 */
class FibreRenderer
{
    public:
        /**
         * \brief Renders a fibre.
         * \param b The fibre to be rendered.
         */
        virtual void render(Box& b) = 0;
};

#endif
