#ifndef BOX_RENDERER_HPP
#define BOX_RENDERER_HPP

#include "Box.hpp"

/**
 * \brief Handles rendering a \c Box.
 */
class BoxRenderer
{
    public:
        /**
         * \brief Renders a box.
         * \param b The box to be rendered.
         */
        virtual void render(Box& b) = 0;
};

#endif
