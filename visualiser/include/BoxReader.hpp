#ifndef BOX_READER_HPP
#define BOX_READER_HPP

#include "Box.hpp"

#include <memory>
#include <string>
#include <fstream>

/**
 * \brief Handles reading data about a \c Box instance from a file.
 */
class BoxReader
{
    public:
        /**
         * \brief Reads details about a \c Box instance and the fibres contained within it from a
         * file with the given filename.
         *
         * \param filename The name of the file that is to be read from.
         *
         * \return A pointer to the box.
         */
        virtual std::unique_ptr<Box> read(const std::string& filename) = 0;

    protected:
        std::ifstream file; ///< The file stream that is read from.
};

#endif
