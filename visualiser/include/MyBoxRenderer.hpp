#ifndef MY_BOX_RENDERER_HPP
#define MY_BOX_RENDERER_HPP

#include "BoxRenderer.hpp"

/**
 * \brief Renders a simple white box in 3D space.
 */
class MyBoxRenderer : public BoxRenderer
{
    public:
        /**
         * \brief A concrete implementation of the pure virtual function declared by \c BoxRenderer.
         * \param b The box to be rendered.
         */
        void render(Box& b);
};

#endif
