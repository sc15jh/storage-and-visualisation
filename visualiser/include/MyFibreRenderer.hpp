#ifndef MY_FIBRE_RENDERER_HPP
#define MY_FIBRE_RENDERER_HPP

#include "FibreRenderer.hpp"

/**
 * \brief Renders fibres as line segments in 3D space, with their color changing with depth.
 */
class MyFibreRenderer : public FibreRenderer
{
    public:
        /**
         * \brief A concrete implementation of the pure virtual function declared by \c FibreRenderer.
         * \param b The box to be rendered.
         */
        void render(Box& b);
};

#endif
