# Visualiser
This program handles the visualisation of a fibre network. It allows the user to move around in 2D/3D space
to explore the fibre network.

## How to use
If the fibre network generated is 3-dimensional, the mouse is used to rotate the cube in 3D space. The left mouse button must be held down, and then rotation works with mouse movement as expected.

### Keys
`S` - Screenshot

`]` - Zoom in

`[` - Zoom out

`◀` - Pan left

`▶` - Pan right

`▲` - Pan up

`▼` - Pan down

## Building
```
make
```
The `Makefile` is designed to work for building on OS X and Linux. This produces an executable called `visualiser.out`. Using the `-j` flag may improve compile times by allowing make to use multiple threads. 

### Requisites
* OpenGL (≥ 3.2)
* GLUT
* GLU
* libpng

##  Running
The program is executed like so:
```
./visualiser.out myinput.dat
```
It may be useful to visualise both the original fibre network and the connected fibre network side-by-side.
This little script will do just that.
```bash
#!/bin/bash
./visualiser.out $1 &
./visualiser.out $1.con &
```
The script is then run by executing `./sidebyside.sh inputfile.dat`, which will start two instances of the program.
