#ifndef FIBRE_GENERATOR_HPP
#define FIBRE_GENERATOR_HPP

#include "Box.hpp"

/**
 * \brief An abstract class representing a generator of fibres to be put inside a box.
 */
class FibreGenerator
{
    public:
        /**
         * \brief Generates a network of fibres and puts it in \c box.
         *
         * \param box The box that is to contain the generated network.
         * \param num The number of fibres to generate.
         * \param length The length of the fibres.
         * \param radius The radius of the fibres.
         */
        virtual void generate(Box& box, unsigned num, float length, float radius) = 0;
};

#endif
