#ifndef PLAINTEXT_BOX_WRITER_HPP
#define PLAINTEXT_BOX_WRITER_HPP

#include "BoxWriter.hpp"

/**
 * \brief Handles writing plaintext data about a \c Box instance to a file
 */
class PlaintextBoxWriter : public BoxWriter
{
    public:
        /**
         * \brief A concrete implementation of the pure virtual function declared
         * by \c BoxWriter.
         *
         * \param box The box that is to be written.
         * \param filename The name of the file that is to be written to.
         */
        void write(Box& box, const std::string& filename);

    private:
        /**
         * \brief Writes each fibre to the file.
         * \param box The box containing the fibres.
         */
        void writeFibres(Box& box);

        /**
         * \brief Writes each fibres' nodes to the file.
         * \param box The box containing the fibres.
         */
        void writeNodes(Box& box);
};

#endif
