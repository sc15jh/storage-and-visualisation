#ifndef BOX_WRITER_HPP
#define BOX_WRITER_HPP

#include "Box.hpp"

#include <string>
#include <fstream>

/**
 * \brief Handles writing data about a \c Box instance to a file.
 */
class BoxWriter
{
    public:
        /**
         * \brief Writes details about \c box and the fibres contained within it to a
         * file with the given filename.
         *
         * \param box The box that is to be written.
         * \param filename The name of the file that is to be written to.
         */
        virtual void write(Box& box, const std::string& filename) = 0;

    protected:
        std::ofstream file; ///< The file stream that is written to.
};

#endif
