#ifndef RNODE_HPP
#define RNODE_HPP

#include "Fibre.hpp"

#include <glm/glm.hpp>

#define MAX_ENTRIES 5
#define MIN_ENTRIES int(MAX_ENTRIES / 2)

struct MBR
{
    glm::vec3 a;
    glm::vec3 b;
};

struct RNode
{
    RNode* child[MAX_ENTRIES];
    MBR mbr;
    Fibre* fibre;
};

#endif
