#include "ShortestDistanceConnector.hpp"

#include <map>
#include <vector>
#include <unordered_set>

/**
 * \brief Adds nodes at the point of intersection of \c f1 and \c f2, if there is one.
 *
 * \param box The box containing the fibres.
 * \param epsilon Unused in this function.
 * \param connectingFibreRadius Unused in this function.
 * \param f1 The first fibre.
 * \param f2 The second fibre.
 * \param newFibres Unused in this function.
 */
void connectPair2D(Box& box, float epsilon, float connectingFibreRadius, Fibre& f1, Fibre& f2, std::vector<Fibre>& newFibres)
{
    float len1 = glm::distance(f1.start, f1.end);
    float len2 = glm::distance(f2.start, f2.end);

    glm::vec3 deltaS = box.translateForPBC(f2.start - f1.start);

    glm::vec3 u = glm::normalize(f1.end - f1.start);
    glm::vec3 v = glm::normalize(f2.end - f2.start);

    float numerator = (deltaS.x * u.y) / (u.x * v.y) - (deltaS.y / v.y);
    float denominator = 1.f - (v.x * u.y) / (u.x * v.y);

    float mu = numerator / denominator;
    float lambda = (deltaS.x + mu * v.x) / u.x;

    // Line segments do not intersect, therefore return.
    if (lambda < 0 || lambda > len1 || mu < 0 || mu > len2)
        return;

    glm::vec3 intersection = f1.start + lambda * u;

    // Create two nodes at the intersection point, ensuring they have the same ID.
    Node node1(intersection);
    Node node2(intersection, node1.getID());

    f1.addNode(node1);
    f2.addNode(node2);

    // Add these nodes to the box's list of nodes.
    box.addNode(node1);
    box.addNode(node2);

    // Update these two fibres in the box's simple list.
    box.getFibres()[f1.getID()] = f1;
    box.getFibres()[f2.getID()] = f2;
}

/**
 * \brief Connects \c f1 and \c f2 with a connecting fibre if they are close enough.
 *
 * \param box The box containing the fibres.
 * \param epsilon The maximum distance between two fibres that can be connected.
 * \param connectingFibreRadius The radius of the connecting fibres.
 * \param f1 The first fibre.
 * \param f2 The second fibre.
 * \param newFibres A list of the connecting fibres to be added to the box.
 */
void connectPair3D(Box& box, float epsilon, float connectingFibreRadius, Fibre& f1, Fibre& f2, std::vector<Fibre>& newFibres)
{
    glm::vec3 f1S = f1.start, f1E = f1.end;
    glm::vec3 n1 = glm::normalize(f1E - f1S);
    float len1 = glm::distance(f1S, f1E);

    glm::vec3 f2S = f2.start, f2E = f2.end;
    glm::vec3 n2 = glm::normalize(f2E - f2S);
    float len2 = glm::distance(f2S, f2E);

    glm::vec3 deltaS = box.translateForPBC(f2S - f1S);

    float a = glm::dot(n1, n2);
    float b = glm::dot(n1, deltaS);
    float c = glm::dot(n2, deltaS);

    float mu = (c - a*b) / (a*a);
    float lambda = (a*mu + b);

    glm::vec3 p1 = f1S + lambda * n1;
    glm::vec3 p2 = f2S + mu * n2;
    glm::vec3 deltaP = box.translateForPBC(p2 - p1);

    // Ensure that this point is actually on f1.
    if (lambda < 0)
        p1 = f1S;
    else if (lambda > len1)
        p1 = f1E;

    // Ensure that this point is actually on f2.
    if (mu < 0)
        p2 = f2S;
    else if (mu > len2)
        p2 = f2E;

    // If these two points are close enough, create a connecting
    // fibre between the two points, making each point a node.
    if (glm::length(deltaP) <= epsilon)
    {
        Fibre connectingFibre(p1, p2, connectingFibreRadius);

        // Create nodes at the connection points, making sure that nodes
        // at the same position have the same ID.
        Node node1(p1), node2(p2);
        Node node3(p1, node1.getID()), node4(p2, node2.getID());

        // Add these nodes to the box's list of nodes.
        box.addNode(node1);
        box.addNode(node2);
        box.addNode(node3);
        box.addNode(node4);

        f1.addNode(node1);
        f2.addNode(node2);
        connectingFibre.addNode(node3);
        connectingFibre.addNode(node4);
        connectingFibre.setConnectingFibre();

        newFibres.push_back(connectingFibre);

        // Update these two fibres in the box's simple list.
        box.getFibres()[f1.getID()] = f1;
        box.getFibres()[f2.getID()] = f2;
    }
}

void ShortestDistanceConnector::connect(Box& b, float epsilon, float connectingFibreRadius)
{
    std::vector<Fibre> newFibres;

    auto& cellList = b.getCellList();
    unsigned numCellsX = cellList.getNumCellsX();
    unsigned numCellsY = cellList.getNumCellsY();
    unsigned numCellsZ = cellList.getNumCellsZ();

    // Iterate over each cell in the box's cell list. For each cell, check
    // each pair of fibres in that cell and the neighbouring cells.
    for (unsigned z = 0; z <= numCellsZ; ++z)
    {
        for (unsigned y = 0; y <= numCellsY; ++y)
        {
            for (unsigned x = 0; x <= numCellsX; ++x)
            {
                // Get the index of the next cell along the x/y/z axis, wrapping around
                // if we are at the last cell along this axis.
                unsigned xMod = (x+1) % numCellsX;
                unsigned yMod = (y+1) % numCellsY;
                unsigned zMod = (z+1) % numCellsZ;

                auto& cur       = cellList.getCell(x   , y   , z    );
                auto& right     = cellList.getCell(xMod, y   , z    );
                auto& rightIn   = cellList.getCell(xMod, y   , zMod);
                auto& upRightIn = cellList.getCell(xMod, yMod, zMod);
                auto& upRight   = cellList.getCell(xMod, yMod, z   );
                auto& up        = cellList.getCell(x   , yMod, z   );
                auto& upIn      = cellList.getCell(x   , yMod, zMod);
                auto& in        = cellList.getCell(x   , y   , zMod);

                // Get the appropriate connector function given the dimensionality of the box.
                auto connectorFunc = (b.is2D() ? connectPair2D : connectPair3D);

                for (auto& f1 : cur)
                {
                    for (auto& f2 : right)
                        if (numCellsX > 2)
                            connectorFunc(b, epsilon, connectingFibreRadius, f1, f2, newFibres);

                    for (auto& f2 : rightIn)
                        if (numCellsX > 2 && numCellsZ > 2)
                            connectorFunc(b, epsilon, connectingFibreRadius, f1, f2, newFibres);

                    for (auto& f2 : upRightIn)
                        if (numCellsX > 2 && numCellsY > 2 && numCellsZ > 2)
                            connectorFunc(b, epsilon, connectingFibreRadius, f1, f2, newFibres);

                    for (auto& f2 : upRight)
                        if (numCellsX > 2 && numCellsY > 2)
                            connectorFunc(b, epsilon, connectingFibreRadius, f1, f2, newFibres);

                    for (auto& f2 : up)
                        if (numCellsY > 2)
                            connectorFunc(b, epsilon, connectingFibreRadius, f1, f2, newFibres);

                    for (auto& f2 : upIn)
                        if (numCellsY > 2 && numCellsZ > 2)
                            connectorFunc(b, epsilon, connectingFibreRadius, f1, f2, newFibres);

                    for (auto& f2 : in)
                        if (numCellsZ > 2)
                            connectorFunc(b, epsilon, connectingFibreRadius, f1, f2, newFibres);
                }

                // Check every pair in the current cell separately to make sure we don't check
                // duplicate pairs.
                for (unsigned i = 0; i < cur.size(); ++i)
                    for (unsigned j = i + 1; j < cur.size(); ++j)
                        connectorFunc(b, epsilon, connectingFibreRadius, cur[i], cur[j], newFibres);
            }
        }
    }

    // Add the new connecting fibres we created.
    for (auto& f : newFibres)
        b.addFibre(f);
}

void ShortestDistanceConnector::connectSLOW(Box& b, float epsilon, float connectingFibreRadius)
{
    std::vector<Fibre> newFibres;

    // Get the appropriate connector function given the dimensionality of the box.
    auto connectorFunc = (b.is2D() ? connectPair2D : connectPair3D);

    auto& fibres = b.getFibres();
    for (unsigned i = 0; i < fibres.size(); ++i)
        for (unsigned j = i + 1; j < fibres.size(); ++j)
            connectorFunc(b, epsilon, connectingFibreRadius, fibres[i], fibres[j], newFibres);

    // Add the new connecting fibres we created.
    for (auto& f : newFibres)
        b.addFibre(f);
}
