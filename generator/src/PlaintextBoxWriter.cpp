#include "PlaintextBoxWriter.hpp"

void PlaintextBoxWriter::writeFibres(Box& box)
{
    unsigned numCellsX = box.getCellList().getNumCellsX();
    unsigned numCellsY = box.getCellList().getNumCellsY();
    unsigned numCellsZ = box.getCellList().getNumCellsZ();

    // For each cell in the cell list.
    for (unsigned x = 0; x < numCellsX; ++x)
    {
        for (unsigned y = 0; y < numCellsY; ++y)
        {
            for (unsigned z = 0; z < numCellsZ; ++z)
            {
                // Get a list of the fibres in this cell.
                auto& fibresInCell = box.getCellList().getCell(x, y, z);

                // Write each fibre in this cell.
                for (auto& fibre : fibresInCell)
                    file << fibre << std::endl;
            }
        }
    }
}

void PlaintextBoxWriter::writeNodes(Box& box)
{
    unsigned numCellsX = box.getCellList().getNumCellsX();
    unsigned numCellsY = box.getCellList().getNumCellsY();
    unsigned numCellsZ = box.getCellList().getNumCellsZ();

    // For each cell in the cell list.
    for (unsigned x = 0; x < numCellsX; ++x)
    {
        for (unsigned y = 0; y < numCellsY; ++y)
        {
            for (unsigned z = 0; z < numCellsZ; ++z)
            {
                // Get a list of the fibres in this cell.
                auto& fibresInCell = box.getCellList().getCell(x, y, z);

                // For each fibre in this cell.
                for (auto& fibre : fibresInCell)
                {
                    // For each node on this fibre.
                    for (auto& node : fibre.getNodes())
                    {
                        // Write the ID and position of this node.
                        glm::vec3 pos = node.getPosition();
                        file << "n " << node.getID() << " " << pos.x << " " << pos.y << " " << pos.z;

                        // Write the parent fibres of this node.
                        for (auto parentID : node.getParents())
                            file << " " << parentID;

                        file << std::endl;
                    }
                }
            }
        }
    }
}

void PlaintextBoxWriter::write(Box& box, const std::string& filename)
{
    file.open(filename);

    if (!file.is_open())
    {
        std::cerr << "PlaintextBoxWriter::write: unable to open file \"" << filename << "\"" << std::endl;
        return;
    }

    file << box << std::endl;

    writeFibres(box);
    writeNodes(box);

    file.close();
}
