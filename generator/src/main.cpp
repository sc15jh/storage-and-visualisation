#include "Box.hpp"
#include "Node.hpp"
#include "BoxWriter.hpp"
#include "PlaintextBoxWriter.hpp"
#include "RandomFibreGenerator.hpp"
#include "ShortestDistanceConnector.hpp"

#include <memory>

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " filename" << std::endl;
        return 1;
    }

    const float fibreLength = 1.414213562f;

    Box b({10, 10, 0}, fibreLength);
    std::unique_ptr<FibreGenerator> generator(new RandomFibreGenerator);
    std::unique_ptr<BoxWriter> writer(new PlaintextBoxWriter);
    std::unique_ptr<ShortestDistanceConnector> connector(new ShortestDistanceConnector);

    Fibre f1({0,0,0}, {1,1,0}, 1.f);
    Fibre f2({0,1,0}, {1,0,0}, 1.f);
    b.addFibre(f1);
    b.addFibre(f2);

    //generator->generate(b, 50, fibreLength, 1.f);
    writer->write(b, argv[1]);

    connector->connectSLOW(b, .05f, 1.f);

    writer->write(b, std::string(argv[1]) + ".con");

    for (auto& f : b.getFibres())
        std::cout << f << std::endl;

    for (auto& n : b.getNodes())
        std::cout << n << std::endl;

    std::cout << std::boolalpha << b.areListsConsistent() << std::endl;

    //for (auto& fibre : b.getFibres())
    //{
    //    std::cout << fibre << std::endl;

    //    for (auto& node : fibre.getNodes())
    //    {
    //        std::cout << "\t" << node << "\t:\t";

    //        std::pair<Node*,Node*> leftRight = fibre.getAdjacentNodes(node);
    //        std::cout << leftRight.first << " " << leftRight.second << std::endl;
    //    }
    //}

    return 0;
}
