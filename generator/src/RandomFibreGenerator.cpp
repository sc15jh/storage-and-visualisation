#include "RandomFibreGenerator.hpp"

#include <cmath>
#include <random>

std::random_device RandomFibreGenerator::rd;
std::default_random_engine RandomFibreGenerator::generator(rd());
std::uniform_real_distribution<float> RandomFibreGenerator::distribution(0.f, 1.f);

void RandomFibreGenerator::generate(Box& b, unsigned num, float length, float radius)
{
    for (unsigned i = 0; i < num; ++i)
    {
        glm::vec3 start = randomPointWithinBox(b);
        glm::vec3 direction = randomUnitVector(b);
        glm::vec3 end = start + direction * length;

        Fibre fibre(start, end, radius);
        b.addFibre(fibre);
    }
}

glm::vec3 RandomFibreGenerator::randomUnitVector(const Box& b) const
{
    return (b.getDepth() == 0 ? random2DVector() : random3DVector());
}

glm::vec3 RandomFibreGenerator::random2DVector() const
{
    float theta = 2.f * M_PI * distribution(generator);

    return { std::sin(theta), std::cos(theta), 0.f };
}

glm::vec3 RandomFibreGenerator::random3DVector() const
{
    float phi = 2.f * M_PI * distribution(generator);
    float rho = 2.f * distribution(generator) - 1.f;
    float sq = std::sqrt(1.f - rho * rho);

    return { std::cos(phi) * sq, std::sin(phi) * sq, rho };
}

glm::vec3 RandomFibreGenerator::randomPointWithinBox(const Box& b) const
{
    glm::vec3 point =
    {
        b.getWidth() * distribution(generator),
        b.getHeight() * distribution(generator),
        b.getDepth() * distribution(generator)
    };

    return point;
}
