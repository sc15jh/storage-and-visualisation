#include "Fibre.hpp"

#include <glm/glm.hpp>
#include <gtest/gtest.h>

TEST(FibreTest, Constructor)
{
    Fibre f(glm::vec3(1.0, 2.0, 3.0), glm::vec3(4.0, 5.0, 6.0), 10.f);

    EXPECT_EQ(glm::vec3(1.0, 2.0, 3.0), f.getStart());
    EXPECT_EQ(glm::vec3(4.0, 5.0, 6.0), f.getEnd());
    EXPECT_EQ(10.f, f.getRadius());
}

TEST(FibreTest, FibreIDs)
{
    Fibre ourFirst({1,2,3}, {4,5,6}, 1.f);
    unsigned ourFirstID = ourFirst.getID();

    const unsigned numToAdd = 10;
    for (unsigned i = 0; i < numToAdd; ++i)
        Fibre({1,2,3}, {4,5,6}, 1.f);

    Fibre ourLast({1,2,3}, {4,5,6}, 1.f);
    unsigned ourLastID = ourLast.getID();

    EXPECT_EQ(ourLastID, ourFirstID + numToAdd + 1);
}

TEST(FibreTest, CopiedFibreID)
{
    Fibre f1({1,2,3}, {4,5,6}, 1.f);
    Fibre f2(f1);

    EXPECT_EQ(f2.getID(), f1.getID());
}
