#include "Box.hpp"

#include <glm/glm.hpp>
#include <gtest/gtest.h>

TEST(BoxTest, Constructor)
{
    Box b(10, 20, 30);

    EXPECT_EQ(10, b.getWidth());
    EXPECT_EQ(20, b.getHeight());
    EXPECT_EQ(30, b.getDepth());
}

TEST(BoxTest, AddFibre)
{
    Box b(10, 20, 30);
    b.addFibre(Fibre({1,2,3}, {4,5,6}, 10));

    EXPECT_EQ(1, b.numFibres());
}

TEST(BoxTest, GetFibres)
{
    Box b(10, 20, 30);

    Fibre f({1,2,3}, {4,5,6}, 10);
    unsigned fID = f.getID();
    b.addFibre(f);

    auto& fibres = b.getFibres();

    EXPECT_EQ(fibres[0].getID(), fID);
}
