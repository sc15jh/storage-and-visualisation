#include "RandomFibreGenerator.hpp"

#include <gtest/gtest.h>

/* 
 * Check if, for each end point of the fibre:
 *      0 <= x <= 10
 *      0 <= y <= 20
 *      0 <= z <= 30
 */
TEST(RandomFibreGeneratorTest, RandomPointWithinBox)
{
    Box box(10, 20, 30);
    RandomFibreGenerator gen;

    gen.generate(box);

    Fibre firstFibre = box.getFibres()[0];

    // Start point.
    EXPECT_GE(firstFibre.getStart().x, 0);
    EXPECT_GE(firstFibre.getStart().y, 0);
    EXPECT_GE(firstFibre.getStart().z, 0);
    EXPECT_LE(firstFibre.getStart().x, 10);
    EXPECT_LE(firstFibre.getStart().y, 20);
    EXPECT_LE(firstFibre.getStart().z, 30);

    // End point.
    EXPECT_GE(firstFibre.getEnd().x, 0);
    EXPECT_GE(firstFibre.getEnd().y, 0);
    EXPECT_GE(firstFibre.getEnd().z, 0);
    EXPECT_LE(firstFibre.getEnd().x, 10);
    EXPECT_LE(firstFibre.getEnd().y, 20);
    EXPECT_LE(firstFibre.getEnd().z, 30);
}
