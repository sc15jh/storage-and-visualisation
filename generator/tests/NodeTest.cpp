#include "Node.hpp"

#include <glm/glm.hpp>
#include <gtest/gtest.h>

TEST(NodeTest, CtorDefault)
{
    Node n;

    EXPECT_EQ(0.0, n.getPosition().x);
    EXPECT_EQ(0.0, n.getPosition().y);
    EXPECT_EQ(0.0, n.getPosition().z);
}

TEST(NodeTest, CtorElementWise)
{
    Node n(12.0, 24.0, 36.0);

    EXPECT_EQ(12.0, n.getPosition().x);
    EXPECT_EQ(24.0, n.getPosition().y);
    EXPECT_EQ(36.0, n.getPosition().z);
}

TEST(NodeTest, CtorVector)
{
    Node n(glm::vec3(12.0, 24.0, 36.0));

    EXPECT_EQ(12.0, n.getPosition().x);
    EXPECT_EQ(24.0, n.getPosition().y);
    EXPECT_EQ(36.0, n.getPosition().z);
}
