#include "Box.hpp"

Box::Box(glm::vec3 dimensions, float fibreLength)
    : width(dimensions.x),
      height(dimensions.y),
      depth(dimensions.z),
      periodicX(false),
      periodicY(false),
      periodicZ(false),
      cellList(width, height, depth, fibreLength)
{
}

bool Box::areListsConsistent() const
{
    // For each node in the box.
    for (const auto& node : nodes)
    {
        // For each parent of this node.
        for (const auto& parentID : node.getParents())
        {
            // Get this parent fibre.
            const auto& parentFibre = fibres[parentID];

            // Iterate through each node that this fibre owns. If this node
            // isn't found, this lists aren't consistent and so we return false.
            bool ownsNode = false;
            for (const auto& child : parentFibre.getNodes())
                if (child.getID() == node.getID())
                    ownsNode = true;

            // This fibre should contain node but it doesn't, so return false.
            if (!ownsNode)
                return false;
        }
    }

    // We now do the same but in reverse.
    // For each fibre in the box.
    for (const auto& fibre : fibres)
    {
        // For each node that this fibre owns.
        for (const auto& node : fibre.getNodes())
        {
            // Does this node say that it has this fibre as a parent?
            bool ownedByFibre = false;
            for (const auto& parentID : node.getParents())
                if (parentID == fibre.getID())
                    ownedByFibre = true;

            if (!ownedByFibre)
                return false;
        }
    }

    // If we haven't returned by this point then the lists are consistent.
    return true;
}

/**
 * \brief Get the translation direction for a fibre.
 *
 * \param delta The distance between two points in some axis.
 * \param max The size of the box in some axis.
 *
 * \return 1 for right, -1 for left, and 0 for no translation.
 */
int translationDirection(float delta, float max)
{
    if (delta >= 0.5f * max)
        return -1;
    else if (delta < -0.5f * max)
        return 1;
    else
        return 0;
}

/**
 * \brief Translate the point \c deltaS by \c width in the x-axis.
 *
 * \param deltaS Vector between two points.
 * \param width The width of the box.
 */
void translateInX(glm::vec3& deltaS, float width)
{
    deltaS.x += width * translationDirection(deltaS.x, width);
}

/**
 * \brief Translate the point \c deltaS by \c height in the y-axis.
 *
 * \param deltaS Vector between two points.
 * \param height The height of the box.
 */
void translateInY(glm::vec3& deltaS, float height)
{
    deltaS.y += height * translationDirection(deltaS.y, height);
}

/**
 * \brief Translate the point \c deltaS by \c depth in the z-axis.
 *
 * \param deltaS Vector between two points.
 * \param depth The depth of the box.
 */
void translateInZ(glm::vec3& deltaS, float depth)
{
    deltaS.z += depth * translationDirection(deltaS.z, depth);
}

glm::vec3 Box::translateForPBC(glm::vec3 p)
{
    glm::vec3 pt = p;
    if (periodicX) translateInX(pt, width);
    if (periodicY) translateInY(pt, height);
    if (periodicZ) translateInZ(pt, depth);

    return pt;
}

float Box::distance(glm::vec3 p1, glm::vec3 p2)
{
    glm::vec3 delta = p2 - p1;

    return glm::length(translateForPBC(delta));
}

void Box::restrictToBoundaries(Fibre& fibre)
{
    auto addBoundaryNode = [&](glm::vec3& pos)
    {
        Node n(pos);
        n.setBoundaryNode();
        fibre.addNode(n);
        nodes.push_back(n);
    };

    // Do we need to add a node at the point of truncation to this fibre?
    bool needToAddNode = false;

    if (!periodicX)
    {
        if (fibre.end.x < 0)
        {
            fibre.end.x = 0.f;
            needToAddNode = true;
        }
        else if (fibre.end.x > width)
        {
            fibre.end.x = width;
            needToAddNode = true;
        }
    }

    if (!periodicY)
    {
        if (fibre.end.y < 0)
        {
            fibre.end.y = 0;
            needToAddNode = true;
        }
        else if (fibre.end.y > height)
        {
            fibre.end.y = height;
            needToAddNode = true;
        }
    }

    if (!periodicZ)
    {
        if (fibre.end.z < 0)
        {
            fibre.end.z = 0;
            needToAddNode = true;
        }
        else if (fibre.end.z > depth)
        {
            fibre.end.z = depth;
            needToAddNode = true;
        }
    }

    if (needToAddNode)
        addBoundaryNode(fibre.end);
}

void Box::addNode(Node node)
{
    if (node.getID() >= nodes.size())
        nodes.push_back(node);
}

std::vector<Node>& Box::getNodes()
{
    return nodes;
}

void Box::addFibre(Fibre fibre)
{
    restrictToBoundaries(fibre);
    cellList.insert(fibre);
    fibres.push_back(fibre);
}

void Box::setWidth(float width)
{
    this->width = width;
}

void Box::setHeight(float height)
{
    this->height = height;
}

void Box::setDepth(float depth)
{
    this->depth = depth;
}

void Box::setPeriodicX(bool flag)
{
    periodicX = flag;
}

void Box::setPeriodicY(bool flag)
{
    periodicY = flag;
}

void Box::setPeriodicZ(bool flag)
{
    periodicZ = flag;
}

float Box::getWidth() const
{
    return width;
}

float Box::getHeight() const
{
    return height;
}

float Box::getDepth() const
{
    return depth;
}

bool Box::isPeriodicX() const
{
    return periodicX;
}

bool Box::isPeriodicY() const
{
    return periodicY;
}

bool Box::isPeriodicZ() const
{
    return periodicZ;
}

bool Box::is2D() const
{
    return getDepth() == 0.f;
}

CellList& Box::getCellList()
{
    return cellList;
}

std::vector<Fibre>& Box::getFibres()
{
    // Sort by ID before returning.
    std::sort(fibres.begin(), fibres.end());
    return fibres;
}

std::ostream& operator<<(std::ostream& out, const Box& box)
{
    out << "b " << box.getWidth() << " " << box.getHeight() << " " << box.getDepth()
        << std::noboolalpha << " " << box.isPeriodicX() << " " << box.isPeriodicY() << " " << box.isPeriodicZ();

    return out;
}
