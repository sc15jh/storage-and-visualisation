#include "Fibre.hpp"

#include <limits>

Fibre::Fibre(const glm::vec3& start, const glm::vec3& end, float radius)
    : start(start),
      end(end),
      radius(radius),
      connectingFibre(false)
{
    static unsigned idToBeAssigned = 0;

    id = idToBeAssigned++;
}

void Fibre::setRadius(float radius)
{
    this->radius = radius;
}

std::vector<Node>::iterator Fibre::getNodeInsertionPoint(Node& node)
{
    auto it = nodes.begin();

    for (; it != nodes.end(); ++it)
    {
        float thisNodeDist = glm::distance(start, it->getPosition());
        float newNodeDist = glm::distance(start, node.getPosition());

        if (newNodeDist < thisNodeDist)
            break;
    }

    return it;
}

void Fibre::addNode(Node& node)
{
    node.addParent(*this);
    auto it = getNodeInsertionPoint(node);
    nodes.insert(it, node);
}

float Fibre::getRadius() const
{
    return radius;
}

unsigned Fibre::getID() const
{
    return id;
}

std::pair<Node*, Node*> Fibre::getAdjacentNodes(const Node& n)
{
    Node* nodeBefore = nullptr;
    Node* nodeAfter = nullptr;

    for (unsigned i = 0; i < nodes.size(); ++i)
    {
        // If we've found the node n.
        if (nodes[i].getID() == n.getID())
        {
            // Get the nodes before and after n, if they exist.
            if (i > 0)
                nodeBefore = &nodes[i - 1];
            if (i < nodes.size() - 1)
                nodeAfter = &nodes[i + 1];

            // Our job is done.
            break;
        }
    }

    return std::make_pair(nodeBefore, nodeAfter);
}

const std::vector<Node>& Fibre::getNodes() const
{
    return nodes;
}

void Fibre::setConnectingFibre()
{
    connectingFibre = true;
}

bool Fibre::isConnectingFibre() const
{
    return connectingFibre;
}

bool Fibre::operator<(const Fibre& other) const
{
    return id < other.getID();
}

std::ostream& operator<<(std::ostream& out, const Fibre& fibre)
{
    out << "f " << fibre.getID() << " " << fibre.start.x << " " << fibre.start.y << " " << fibre.start.z
        << " " << fibre.end.x << " " << fibre.end.y << " " << fibre.end.z << " " << fibre.getRadius();

    return out;
}
