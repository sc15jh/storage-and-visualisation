#include "Node.hpp"
#include "Fibre.hpp"

unsigned Node::idToBeAssigned = 0;

Node::Node(const glm::vec3& position)
    : Node(position, idToBeAssigned++)
{
}

Node::Node(const glm::vec3& position, unsigned id)
    : id(id),
      position(position),
      boundaryNode(false)
{
}

void Node::addParent(const Fibre& fibre)
{
    parents.push_back(fibre.getID());
}

glm::vec3 Node::getPosition() const
{
    return position;
}

unsigned Node::getID() const
{
    return id;
}

const std::vector<unsigned>& Node::getParents() const
{
    return parents;
}


void Node::setBoundaryNode()
{
    boundaryNode = true;
}

bool Node::isBoundaryNode() const
{
    return boundaryNode;
}

std::ostream& operator<<(std::ostream& out, const Node& node)
{
    out << "n " << node.getID() << " " << node.getPosition().x << " " << node.getPosition().y << " " << node.getPosition().z;

    return out;
}
