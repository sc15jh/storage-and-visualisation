#ifndef BOX_HPP
#define BOX_HPP

#include "Fibre.hpp"
#include "CellList.hpp"

#include <vector>
#include <iostream>
#include <algorithm>

/**
 * \brief Represents a box in 3D space containing fibres.
 */
class Box
{
    public:
        /**
         * \brief Construct a box with the given dimensions.
         * 
         * \param dimensions The dimensions of the box.
         * \param fibreLength The maximum length of a fibre.
         */
        Box(glm::vec3 dimensions, float fibreLength);

        /**
         * \brief Check whether the list of fibres is consistent with the list of nodes.
         * \return True if the lists are consistent, false otherwise.
         */
        bool areListsConsistent() const;

        /**
         * \brief Translates \c p if PBC's are used.
         *
         * \param p The point to translate.
         *
         * \return The translated point.
         */
        glm::vec3 translateForPBC(glm::vec3 p);

        /**
         * \brief Compute the distance between \c p1 and \c p2, taking into account PBC's.
         *
         * \param p1 First point.
         * \param p2 Second point.
         *
         * \return Distance between \c p1 and \c p2.
         */
        float distance(glm::vec3 p1, glm::vec3 p2);

        /**
         * \brief Adds a fibre into this box.
         * \param fibre The fibre to add to this box.
         */
        void addFibre(Fibre fibre);

        /**
         * \brief Add a node to the list of nodes in this box.
         * \param node The node to add.
         */
        void addNode(Node node);

        /**
         * \brief Set the width of the box.
         * \param width The new width of the box.
         */
        void setWidth(float width);

        /**
         * \brief Set the height of the box.
         * \param height The new height of the box.
         */
        void setHeight(float height);

        /**
         * \brief Set the depth of the box.
         * \param depth The new depth of the box.
         */
        void setDepth(float depth);

        /**
         * \brief Change the periodicity of the x-axis.
         * \param flag The new value for the x-axis periodicity flag.
         */
        void setPeriodicX(bool flag);

        /**
         * \brief Change the periodicity of the y-axis.
         * \param flag The new value for the y-axis periodicity flag.
         */
        void setPeriodicY(bool flag);

        /**
         * \brief Change the periodicity of the z-axis.
         * \param flag The new value for the z-axis periodicity flag.
         */
        void setPeriodicZ(bool flag);

        /**
         * \brief Get the width of the box.
         * \return The width of the box.
         */
        float getWidth() const;

        /**
         * \brief Get the height of the box.
         * \return The height of the box.
         */
        float getHeight() const;

        /**
         * \brief Get the depth of the box.
         * \return The depth of the box.
         */
        float getDepth() const;

        /**
         * \brief Get the value of the x-axis periodicity flag.
         * \return Whether the x-axis is periodic.
         */
        bool isPeriodicX() const;

        /**
         * \brief Get the value of the y-axis periodicity flag.
         * \return Whether the y-axis is periodic.
         */
        bool isPeriodicY() const;

        /**
         * \brief Get the value of the z-axis periodicity flag.
         * \return Whether the z-axis is periodic.
         */
        bool isPeriodicZ() const;

        /**
         * \brief Check if this box is 2-dimensional.
         * \return True if the box is 2-dimensional, false otherwise.
         */
        bool is2D() const;

        /**
         * \brief Get the data structure containing the fibres.
         * \return The cell list containing the fibres.
         */
        CellList& getCellList();

        /**
         * \brief Get the list of fibres in this box.
         * \return The list of fibres in this box.
         */
        std::vector<Fibre>& getFibres();

        /**
         * \brief Get the list of nodes on all the fibres in this box.
         * \return The list of nodes on all the fibres in this box.
         */
        std::vector<Node>& getNodes();

        friend std::ostream& operator<<(std::ostream& out, const Box& box);

    private:
        float width; ///< The width of the box (size in the x-axis).
        float height; ///< The height of the box (size in the y-axis).
        float depth; ///< The depth of the box (size in the z-axis).
        bool periodicX; ///< A flag representing whether the x-axis is periodic.
        bool periodicY; ///< A flag representing whether the y-axis is periodic.
        bool periodicZ; ///< A flag representing whether the z-axis is periodic.

        CellList cellList; ///< Data structure used to store fibres.
        std::vector<Fibre> fibres; ///< Also stores the fibres, just for ease of use on the rendering side.
        std::vector<Node> nodes; ///< A list of all the nodes in the system.

        /**
         * \brief Restricts the \c fibre to the boundaries of the box,
         * taking into account the periodicity of each axis.
         * \param fibre The fibre to modify.
         */
        void restrictToBoundaries(Fibre& fibre);
};

/**
 * \brief Prints the values of a box's members to a given output stream for debugging purposes.
 *
 * \param out The output stream to print to.
 * \param box The box to be printed.
 *
 * \return A reference to the output stream that was printed to.
 */
std::ostream& operator<<(std::ostream& out, const Box& box);

#endif
