#ifndef CELL_LIST_HPP
#define CELL_LIST_HPP

#include "Fibre.hpp"

using CellVector = std::vector<std::vector<std::vector<std::vector<Fibre>>>>;

/**
 * \brief Data structure which stores fibres in cells based on their position.
 */
class CellList
{
    public:
        /**
         * \brief Construct a cell list.
         * 
         * \param width The width of the container.
         * \param height The height of the container.
         * \param depth The depth of the container.
         * \param size The size of the cells.
         */
        CellList(float width, float height, float depth, float size);

        /**
         * \brief Inserts a fibre into the cell list.
         * \param f The fibre to insert.
         */
        void insert(Fibre& f);

        /**
         * \brief Get the number of cells in the x-axis.
         * \return The number of cells.
         */
        unsigned getNumCellsX() const;

        /**
         * \brief Get the number of cells in the y-axis.
         * \return The number of cells.
         */
        unsigned getNumCellsY() const;

        /**
         * \brief Get the number of cells in the z-axis.
         * \return The number of cells.
         */
        unsigned getNumCellsZ() const;

        /**
         * \brief Get a list of fibres in the cell with index (x,y,z).
         * 
         * \param x The x-axis index.
         * \param y The y-axis index.
         * \param z The z-axis index.
         *
         * \return A list of fibres within this cell.
         */
        std::vector<Fibre>& getCell(unsigned x, unsigned y, unsigned z);

        /**
         * \brief Get a list of fibres in the cells referenced by \c indices.
         *
         * \param indices A list of cell indices.
         *
         * \return A list of fibres in the cells referenced by \c indices.
         */
        std::vector<Fibre> getCells(std::vector<glm::vec3> indices);

        friend std::ostream& operator<<(std::ostream& out, const CellList& cellList);

    private:
        float cellSize; ///< The size of each cell.
        unsigned numCellsX; ///< The number of cells in the x-axis.
        unsigned numCellsY; ///< The number of cells in the y-axis.
        unsigned numCellsZ; ///< The number of cells in the z-axis.

        CellVector cells; ///< A 3-dimensional list of fibres.

        std::vector<Fibre> emptyVector; ///< Empty vector to return in case of invalid index.

        /**
         * \brief Is index (x,y,z) valid?
         * \return True if valid, false otherwise.
         */
        bool isIndexValid(unsigned x, unsigned y, unsigned z) const;
};

/**
 * \brief Prints a summary of a cell list for debugging.
 *
 * \param out The output stream to print to.
 * \param cellList The cell list to be printed.
 *
 * \return A reference to the output stream that was printed to.
 */
std::ostream& operator<<(std::ostream& out, const CellList& cellList);

#endif
