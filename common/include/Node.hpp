#ifndef NODE_HPP
#define NODE_HPP

#include <vector>
#include <iostream>
#include <glm/glm.hpp>

/* Forward declare the Fibre class to avoid circular dependencies. */
class Fibre;

/**
 * \brief Represents a node in the network.
 */
class Node
{
    /* Make Fibre a friend so it can access addParent(...). This function should typically not be called by the user. */
    friend class Fibre;

    public:
        /**
         * \brief Initializes the node at the given position vector.
         * \param position The initial position vector of the node.
         */
        Node(const glm::vec3& position);

        /**
         * \brief Initializes the node at the given position vector with the specified id.
         * \param position The initial position vector of the node.
         * \param id The id that should be assigned to this node.
         */
        Node(const glm::vec3& position, unsigned id);

        /**
         * \brief Gets the position vector of the node.
         * \return The position vector of the node.
         */
        glm::vec3 getPosition() const;

        /**
         * \brief Get the unique id of the node.
         * \return The id of this node.
         */
        unsigned getID() const;

        /**
         * \brief Get the parent fibres of this node.
         * \return A list of id's representing this node's parent fibres.
         */
        const std::vector<unsigned>& getParents() const;

        /**
         * \brief Makes this node a boundary node.
         */
        void setBoundaryNode();

        /**
         * \brief Is this node a boundary node?
         * \return True if the node is a boundary node, false otherwise.
         */
        bool isBoundaryNode() const;

        friend std::ostream& operator<<(std::ostream& out, const Node& node);

    private:
        unsigned id; ///< A unique number to identify each instance of this \c Node class.
        glm::vec3 position; ///< The position vector of the node.
        std::vector<unsigned> parents; ///< A list of id's representing this node's parent fibres.
        bool boundaryNode; ///< A bool representing whether this node is a boundary node or not.

        static unsigned idToBeAssigned; ///< The ID that should be assigned to the next-created node.

        /**
         * \brief Add a parent fibre to this node.
         * \param fibre The parent fibre to add to this node.
         */
        void addParent(const Fibre& fibre);
};

/**
 * \brief Prints the values of a node's members to a given output stream for debugging purposes.
 *
 * \param out The output stream to print to.
 * \param node The node to be printed.
 *
 * \return A reference to the output stream that was printed to.
 */
std::ostream& operator<<(std::ostream& out, const Node& node);

#endif
