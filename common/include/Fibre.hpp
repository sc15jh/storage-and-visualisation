#ifndef FIBRE_HPP
#define FIBRE_HPP

#include "Node.hpp"

#include <vector>
#include <iostream>
#include <glm/glm.hpp>

/**
 * \brief Represents an individual fibre in a fibre network.
 */
class Fibre
{
    public:
        /**
         * \brief Constructs a fibre.
         *
         * \param start The position vector for the start of the fibre.
         * \param end The position vector for the end of the fibre.
         * \param radius The radius of the fibre.
         */
        Fibre(const glm::vec3& start, const glm::vec3& end, float radius);

        /**
         * \brief Set the radius of the fibre.
         * \param radius The radius of the fibre.
         */
        void setRadius(float radius);

        /**
         * \brief Adds a node to this fibre.
         * \param node The node to be added.
         */
        void addNode(Node& node);

        /**
         * \brief Get the radius of the fibre.
         * \return The radius of the fibre.
         */
        float getRadius() const;

        /**
         * \brief Get the unique id of the fibre.
         * \return The id of this fibre.
         */
        unsigned getID() const;

        /**
         * \brief Get the list of nodes on this fibre.
         * \return A list of pointers to nodes on this fibre.
         */
        const std::vector<Node>& getNodes() const;

        /**
         * \brief Get the adjacent node to node \c n in each direction.
         * \return Two nodes adjacent to \c n.
         */
        std::pair<Node*, Node*> getAdjacentNodes(const Node& n);

        /**
         * \brief Make this fibre a connecting fibre.
         */
        void setConnectingFibre();

        /**
         * \brief Is this fibre a connecting fibre?
         * \return True if this fibre is a connecting fibre, false otherwise.
         */
        bool isConnectingFibre() const;

        /**
         * \brief Defines the < operator for fibres.
         *
         * A fibre f1 is less than another fibre f2 if f1's ID is less than f2's ID.
         *
         * \return True if this fibre is less than \c other, otherwise false.
         */
        bool operator<(const Fibre& other) const;

        friend std::ostream& operator<<(std::ostream& out, const Fibre& fibre);

    public:
        glm::vec3 start; ///< The start point of the fibre.
        glm::vec3 end; ///< The end point of the fibre.

    private:
        float radius; ///< The radius of the fibre.
        unsigned id; ///< A unique number to identify each instance of this \c Fibre class.
        std::vector<Node> nodes; ///< A list of nodes on this fibre.
        bool connectingFibre; ///< A bool representing whether this fibre is a connecting fibre or not.

        /**
         * \brief Find the position where \c node should be inserted.
         *
         * This method returns the position to insert \c node. The position
         * of nodes in the vector is determined by their physical position.
         *
         * \return The position after where \c node should be inserted.
         */
        std::vector<Node>::iterator getNodeInsertionPoint(Node& node);
};

/**
 * \brief Prints the values of a fibre's members to a given output stream for debugging purposes.
 *
 * \param out The output stream to print to.
 * \param fibre The fibre to be printed.
 *
 * \return A reference to the output stream that was printed to.
 */
std::ostream& operator<<(std::ostream& out, const Fibre& fibre);

#endif
